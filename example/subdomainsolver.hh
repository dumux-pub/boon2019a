// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief Darcy solver class to solve a stationary, single-phase problem.
 */
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

namespace Dumux {

/*!
 * \file
 * \brief Darcy solver class to solve a stationary, single-phase problem.
 * \param TypeTag The type tag of the problem to be solved
 * \LinearSolver Optional argument to define the solver to be used for
 *               the solution of the linear system. We default to the
 *               direct linear solver UMFPack here, but support any other
 *               solver available in Dumux except for the AMGBackend, which
 *               needs additional template parameters and constructor arguments.
 */
template<class TypeTag, class LinearSolver = Dumux::AMGBackend<TypeTag>>
class DarcySolver
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;

    using SolVec = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVars = GetPropType<TypeTag, Properties::GridVariables>;
    using OutputModule = VtkOutputModule<GridVars, SolVec>;

public:
    //! export some data types
    using SolutionVector = SolVec;
    using GridVariables = GridVars;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    void init(const std::string& paramGroup)
    {
        // try to create a grid (from the given grid file or the input file)
        gridManager_.init(paramGroup);

        // we compute on the leaf grid view
        const auto& leafGridView = gridManager_.grid().leafGridView();

        // create the finite volume grid geometry
        fvGridGeometry_ = std::make_shared<GridGeometry>(leafGridView);
        fvGridGeometry_->update();

        // the problem (initial and boundary conditions)
        auto params = std::make_shared<SpatialParams>(fvGridGeometry_, paramGroup);
        problem_ = std::make_shared<Problem>(fvGridGeometry_, params, paramGroup);

        // resize and initialize the given solution vector
        x_ = std::make_shared<SolutionVector>();
        x_->resize(fvGridGeometry_->numDofs());
        problem_->applyInitialSolution(*x_);

        // the grid variables
        gridVariables_ = std::make_shared<GridVariables>(problem_, fvGridGeometry_);
        gridVariables_->init(*x_);

        // initialize the vtk output module
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
        vtkWriter_ = std::make_unique<OutputModule>(*gridVariables_, *x_, problem_->name());
        IOFields::initOutputModule(*vtkWriter_);
        vtkWriter_->addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables_));

        // the assembler without time loop for stationary problem
        assembler_ = std::make_shared<Assembler>(problem_, fvGridGeometry_, gridVariables_);

        // the linear/non-linear solvers
        auto linearSolver = std::make_shared<LinearSolver>(fvGridGeometry_->gridView(), fvGridGeometry_->dofMapper());
        newtonSolver_ = std::make_unique<NewtonSolver>(assembler_, linearSolver);
    }

    //! Solve the system
    void solve()
    {
        newtonSolver_->solve(*x_);
    }

    //! Write current state to disk
    void write(Scalar t)
    {
        vtkWriter_->write(t);
    }

    //! Return the output module
    OutputModule& outputModule() { return *vtkWriter_; }

    //! Return a pointer to the grid geometry
    std::shared_ptr<const GridGeometry> gridGeometryPointer() const { return fvGridGeometry_; }
    const GridGeometry& gridGeometry() const { return *fvGridGeometry_; }

    //! Return reference/pointer to the grid variables
    std::shared_ptr<GridVariables> gridVariablesPointer() { return gridVariables_; }
    const GridVariables& gridVariables() { return *gridVariables_; }

    //! Return reference/pointer to the problem
    std::shared_ptr<Problem> problemPointer() { return problem_; }
    const Problem& problem() const { return *problem_; }

    //! Return reference/pointer to the solution
    std::shared_ptr<SolutionVector> solutionPointer() { return x_; }
    const SolutionVector& solution() const { return *x_; }

private:
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager_;
    std::shared_ptr<GridGeometry> fvGridGeometry_;
    std::shared_ptr<Problem> problem_;
    std::shared_ptr<GridVariables> gridVariables_;
    std::shared_ptr<Assembler> assembler_;

    std::unique_ptr<NewtonSolver> newtonSolver_;
    std::unique_ptr<OutputModule> vtkWriter_;

    std::shared_ptr<SolutionVector> x_;
};

} // end namespace Dumux
