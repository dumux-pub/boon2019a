// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief TODO doc me
 */
#include <config.h>
#include <iostream>
#include <memory>
#include <type_traits>

#include <dune/common/timer.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/foamgrid/foamgrid.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/lagrangedgbasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/integrate.hh>

#include <dumux/discretization/fem/fegridgeometry.hh>

#include "problem.hh"
#include "subdomainsolver.hh"
#include "mortarvariabletype.hh"

#include "interface.hh"
#include "projector.hh"
#include "projectorfactory.hh"
#include "preconditioner.hh"
#include "interfaceoperator.hh"

// Define grid and basis for mortar domain
struct InterfaceTraits
{
    static constexpr int basisOrder = 1;

    using Scalar = double;
    using Grid = Dune::FoamGrid<1, 2>;
    using GridView = typename Grid::LeafGridView;
    using BlockType = Dune::FieldVector<Scalar, 1>;
    using SolutionVector = Dune::BlockVector<BlockType>;
    using FEBasis = Dune::Functions::LagrangeBasis<GridView, basisOrder>;
    using GridGeometry = Dumux::FEGridGeometry<FEBasis>;
    static constexpr Dune::VTK::DataMode VTKDataMode = Dune::VTK::nonconforming;
};

// Solver type
template<class TypeTag>
using Solver = Dumux::DarcySolver<TypeTag>;

// translate mortar variable into variable name for output
std::string getMortarVariableName(Dumux::OnePMortarVariableType mv)
{ return mv == Dumux::OnePMortarVariableType::pressure ? "p" : "flux"; }

/*!
 * \brief TODO doc me.
 */
template< class Projector, class SubDomainSolver >
std::vector< std::shared_ptr<Dumux::MortarInterface<InterfaceTraits, SubDomainSolver, Projector>> >
makeInterfaces( const std::vector<std::shared_ptr<SubDomainSolver>>& subDomainSolvers,
                const std::vector<std::shared_ptr<typename InterfaceTraits::GridGeometry>>& interfaceGridGeometries)
{
    using InterfaceType = Dumux::MortarInterface<InterfaceTraits, SubDomainSolver, Projector>;
    using SDGridGeometry = typename SubDomainSolver::GridGeometry;
    using IFGridGeometry = typename InterfaceTraits::GridGeometry;
    using Glue = Dumux::MultiDomainGlue< typename SDGridGeometry::GridView, typename IFGridGeometry::GridView,
                                         typename SDGridGeometry::ElementMapper, typename IFGridGeometry::ElementMapper>;

    Dune::Timer watch;
    std::cout << "Setting up the interfaces...\n";

    std::size_t ifId = 0;
    std::vector<std::shared_ptr<InterfaceType>> interfaces;
    interfaces.reserve(interfaceGridGeometries.size());
    for (auto ifGGPtr : interfaceGridGeometries)
    {
        int neighborCount = 0;
        std::array<Glue, 2> glues;
        std::array<std::size_t, 2> neighborIds;

        std::size_t solverId = 0;
        for (auto solver : subDomainSolvers)
        {
            auto glue = Dumux::makeGlue(*solver->gridGeometryPointer(), *ifGGPtr);
            if (glue.size() > 0)
            {
                if (neighborCount >= 2)
                    DUNE_THROW(Dune::InvalidStateException, "Each interface is expected to be between only two subdomains");

                glues[neighborCount] = std::move(glue);
                neighborIds[neighborCount] = solverId;
                neighborCount++;
            }

            solverId++;
        }

        if (neighborCount != 2)
            DUNE_THROW(Dune::InvalidStateException, "Each interface is expected to have two neighbors");

        interfaces.emplace_back( std::make_shared<InterfaceType>(ifGGPtr,
                                                                 subDomainSolvers[neighborIds[0]],
                                                                 subDomainSolvers[neighborIds[1]],
                                                                 glues[0], glues[1], ifId) );

        ifId++;
    }

    std::cout << " ... done. Took " << watch.elapsed() << " seconds." << std::endl;

    return interfaces;
}


/////////////////////////////////
// The iterative solve routine //
/////////////////////////////////
template< class Projector, class SubDomainSolver >
void solveIteratively( std::vector<std::shared_ptr<SubDomainSolver>> subDomainSolvers,
                       std::vector<std::shared_ptr<typename InterfaceTraits::GridGeometry>> interfaceGridGeometries,
                       Dumux::OnePMortarVariableType mv )
{
    Dune::Timer watch;

    using namespace Dumux;
    using Interface = MortarInterface< InterfaceTraits, SubDomainSolver, Projector>;
    using InterfaceSolution = typename InterfaceTraits::SolutionVector;
    using InterfaceGridView = typename InterfaceTraits::GridView;
    using InterfaceVTKWriter = Dune::VTKWriter<InterfaceGridView>;
    using InterfaceVTKSequenceWriter = Dune::VTKSequenceWriter<InterfaceGridView>;

    //! create interfaces to all interface geometries
    auto interfaces = makeInterfaces<Projector>(subDomainSolvers, interfaceGridGeometries);

    //! create a solution vector & a vtkWriter for each interface
    std::cout << "\n --- Initializing VTK output --- " << std::endl;
    using namespace Dune::Functions;
    using BlockType = typename InterfaceSolution::block_type;
    using IFGridFunction = std::decay_t<decltype(makeDiscreteGlobalBasisFunction<BlockType>(interfaces[0]->gridGeometry().feBasis(),
                                                                                            InterfaceSolution{}))>;

    std::size_t numMortarDofs = 0;
    const auto numInterfaces = interfaces.size();
    std::vector<InterfaceSolution> interfaceSolutionVectors(numInterfaces);
    std::vector<std::shared_ptr<IFGridFunction>> interfaceGridFunctions(numInterfaces);
    std::vector<std::shared_ptr<InterfaceVTKWriter>> interfaceWriters(numInterfaces);
    for (auto ifPtr : interfaces)
    {
        auto& interface = *ifPtr;
        auto& x = interfaceSolutionVectors[ifPtr->id()];

        const auto numDofs = interface.gridGeometry().numDofs();
        numMortarDofs += numDofs;
        x.resize(numDofs);
        x = 0.0;

        // project initial interface solution (=0.0) to sub domains
        InterfaceSolution p1, p2;
        ProjectionDetail::projectToSubDomain(interface.positiveProjector(), interface.negativeProjector(), x, p1, p2);

        interface.positiveNeighborSolver().problemPointer()->setMortarProjection(interface.id(), p1);
        interface.negativeNeighborSolver().problemPointer()->setMortarProjection(interface.id(), p2);

        // create writer and add interface solution to output
        interfaceWriters[ifPtr->id()] = std::make_shared<InterfaceVTKWriter>(interface.gridGeometry().gridView(), InterfaceTraits::VTKDataMode);

        auto ifVariableGF = makeDiscreteGlobalBasisFunction<BlockType>(interface.gridGeometry().feBasis(), x);
        interfaceGridFunctions[ifPtr->id()] = std::make_shared<IFGridFunction>(ifVariableGF);

        const auto info = Dune::VTK::FieldInfo(getMortarVariableName(mv), Dune::VTK::FieldInfo::Type::scalar, 1);
        if (InterfaceTraits::basisOrder == 0) interfaceWriters[ifPtr->id()]->addCellData(*interfaceGridFunctions[ifPtr->id()], info);
        else interfaceWriters[ifPtr->id()]->addVertexData(*interfaceGridFunctions[ifPtr->id()], info);
    }

    //! create sequence writer for pvd output & write out initial solution
    std::vector<InterfaceVTKSequenceWriter> interfaceSequenceWriters;
    interfaceSequenceWriters.reserve(numInterfaces);
    for (std::size_t i = 0; i < numInterfaces; ++i)
        interfaceSequenceWriters.emplace_back(interfaceWriters[i], getParamFromGroup<std::string>("Interface" + std::to_string(i), "Problem.Name"));

    // create interface operator
    // We currently use the interface solution vector type for the
    // complete mortar space, as MultiTypeBlockVector would require
    // knowing the number of sub-domains at compile time!
    using MortarSolution = InterfaceSolution;
    using Operator = OnePMortarInterfaceOperator<Interface, SubDomainSolver, MortarSolution>;
    Operator op(interfaces, subDomainSolvers, mv);

    // first compute the jump in mortar variable
    std::cout << "\n --- Computing mortar variable jump --- " << std::endl;
    for (auto sdSolverPtr  : subDomainSolvers)
        sdSolverPtr->problemPointer()->setUseHomogeneousSetup(false);

    MortarSolution mortarSolution;
    MortarSolution deltaMortarVariable;
    mortarSolution.resize(numMortarDofs);
    deltaMortarVariable.resize(numMortarDofs);
    mortarSolution = 0.0;
    deltaMortarVariable = 0.0;
    op.apply(mortarSolution, deltaMortarVariable);

    // Solve the homogeneous problem
    std::cout << "\n --- Solving homogeneous problem --- " << std::endl;
    for (auto sdSolverPtr  : subDomainSolvers)
        sdSolverPtr->problemPointer()->setUseHomogeneousSetup(true);

    deltaMortarVariable *= -1.0;
    Dune::InverseOperatorResult result;
    Dumux::OnePMortarPreconditioner<MortarSolution> prec;

    const auto lsType = getParam<std::string>("InterfaceSolver.LinearSolverType");
    const double reduction = getParam<double>("InterfaceSolver.ResidualReduction");
    const std::size_t maxIt = getParam<double>("InterfaceSolver.MaxIterations");
    const int verbosity = getParam<int>("InterfaceSolver.Verbosity");
    if (lsType == "CG")
    {
        Dune::CGSolver<MortarSolution> cgSolver(op, prec, reduction, maxIt, verbosity);
        cgSolver.apply(mortarSolution, deltaMortarVariable, result);
    }
    else if (lsType == "GMRes")
    {
        Dune::RestartedGMResSolver<MortarSolution> gmresSolver(op, prec, reduction, maxIt, maxIt, verbosity);
        gmresSolver.apply(mortarSolution, deltaMortarVariable, result);
    }
    else
        DUNE_THROW(Dune::NotImplemented, "Support for linear solver with name " << lsType);

    if (!result.converged)
        DUNE_THROW(Dune::InvalidStateException, "iterative solver did not converge in " << maxIt << " iterations");

    // solve the sub-domains again to get the right output
    std::cout << "\n --- Computing final solution --- " << std::endl;
    for (auto sdSolverPtr  : subDomainSolvers)
        sdSolverPtr->problemPointer()->setUseHomogeneousSetup(false);
    op.apply(mortarSolution, deltaMortarVariable);

    // extract sub-solutions from global mortar solution
    extractInterfaceSolutions(interfaces, interfaceSolutionVectors, mortarSolution);

    // print time necessary for solve
    std::cout << "\n#####################################################\n\n"
              << "Iterative scheme took " << watch.elapsed() << " seconds\n"
              << "\n#####################################################\n\n";

    // Compute error norms as the sum of all norms of the sub-domains
    std::cout << "\n --- Computing error norms / Writing VTK output --- " << std::endl;

    static constexpr int mortarDim = InterfaceGridView::dimension;
    static constexpr int sdDim = mortarDim+1;

    // For the flux norm, cast the fluxes into an RT0 basis for each subdomain
    using SDGridView = typename SubDomainSolver::GridGeometry::GridView;
    using RT0Basis = RaviartThomasBasis<SDGridView, 0>;
    std::vector< std::shared_ptr<RT0Basis> > sdFluxBases;
    std::vector< std::vector<double> > sdFluxCoefficients;

    double sumSDFluxNorms = 0.0;
    double sumSDIFFluxNorms = 0.0;
    double sumSDPressureNorms = 0.0;

    std::size_t sdId = 0;
    const int order = getParam<int>("L2Error.IntegrationOrder");
    for (auto sdSolverPtr : subDomainSolvers)
    {
        const auto& sdGridGeometry = sdSolverPtr->gridGeometry();
        const auto& sdProblem = sdSolverPtr->problem();
        const auto& x = sdSolverPtr->solution();

        sdFluxBases.emplace_back( std::make_shared<RT0Basis>(sdGridGeometry.gridView()) );
        sdFluxCoefficients.push_back({});
        sdFluxCoefficients.back().resize(sdGridGeometry.gridView().size(1), 0.0);

        typename SubDomainSolver::SolutionVector pExact(sdGridGeometry.gridView().size(0));
        for (const auto& element : elements(sdGridGeometry.gridView()))
        {
            pExact[sdGridGeometry.elementMapper().index(element)] = sdProblem.exact(element.geometry().center());

            auto fluxBasisLocalView = localView(*sdFluxBases.back());
            auto fvGeometry = localView(sdGridGeometry);
            auto elemVolVars = localView(sdSolverPtr->gridVariables().curGridVolVars());
            auto elemFluxVarsCache = localView(sdSolverPtr->gridVariables().gridFluxVarsCache());

            fluxBasisLocalView.bind(element);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            // integrate error in pressure
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& scvGeometry = scv.geometry();
                const auto& rule = Dune::QuadratureRules<double, sdDim>::rule(scvGeometry.type(), order);
                for (const auto& qp : rule)
                {
                    const auto& ipLocal = qp.position();
                    const auto& ipGlobal = scvGeometry.global(ipLocal);
                    const auto error = elemVolVars[scv].pressure() - sdSolverPtr->problem().exact(ipGlobal);
                    sumSDPressureNorms += error*error*scvGeometry.integrationElement(ipLocal)*qp.weight();
                }
            }

            // compute face fluxes
            unsigned int localDofCounter = 0;
            for (const auto& scvf : scvfs(fvGeometry))
            {
                double discreteFlux;
                if (scvf.boundary() && sdSolverPtr->problem().boundaryTypes(element, scvf).hasOnlyNeumann())
                    discreteFlux = sdProblem.neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)
                                   /elemVolVars[scvf.insideScvIdx()].density();
                else
                {
                    typename SubDomainSolver::FluxVariables fluxVars;
                    fluxVars.init(sdProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    discreteFlux = fluxVars.advectiveFlux(0, [] (const auto& vv) { return vv.mobility(); })
                                   /scvf.area();
                }

                // parse into coefficients of the RT0 basis for fluxes
                const auto sign = scvf.boundary() ? 1.0 : (scvf.insideScvIdx() > scvf.outsideScvIdx() ? 1.0 : -1.0);
                sdFluxCoefficients.back()[ fluxBasisLocalView.index(localDofCounter++) ] = discreteFlux*sign;

                // (maybe) compute interface flux error
                if (scvf.boundary() && sdSolverPtr->problem().isOnMortarInterface(element, scvf).first)
                {
                    double scvfError = 0.0;
                    const auto& scvfGeometry = scvf.geometry();
                    const auto& rule = Dune::QuadratureRules<double, mortarDim>::rule(scvfGeometry.type(), order);
                    for (const auto& qp : rule)
                    {
                        const auto& ipLocal = qp.position();
                        const auto& ipGlobal = scvfGeometry.global(ipLocal);
                        const auto error = discreteFlux - sdSolverPtr->problem().exactFlux(ipGlobal)*scvf.unitOuterNormal();
                        scvfError += error*error*qp.weight()*scvfGeometry.integrationElement(ipLocal);
                    }
                    sumSDIFFluxNorms += scvfError;
                }
            }
        }

        // write VTK file with the flux field from the RT0 basis
        auto fluxGf = makeDiscreteGlobalBasisFunction<Dune::FieldVector<double, 2>>(*sdFluxBases.back(), sdFluxCoefficients.back());
        auto fluxZeroGf = makeAnalyticGridViewFunction([&] (const auto& pos) { return sdSolverPtr->problemPointer()->exactFlux(pos); }, sdFluxBases.back()->gridView());
        const auto fluxInfo = Dune::VTK::FieldInfo("RT0Flux", Dune::VTK::FieldInfo::Type::vector, 2);

        const auto sqError = integrateL2Error(sdFluxBases.back()->gridView(), fluxGf, fluxZeroGf, order);
        sumSDFluxNorms += sqError*sqError;

        Dune::VTKWriter<SDGridView> vtkWriterFlux(sdFluxBases.back()->gridView());
        vtkWriterFlux.addCellData(fluxGf, fluxInfo);
        vtkWriterFlux.write("flux_subdomain_" + std::to_string(sdId++) + "_" + getParam<std::string>("Mortar.ProjectorType"));

        // let the problem add the projected mortar variable to output
        sdSolverPtr->problem().addOutputFields(sdSolverPtr->outputModule());

        // add exact pressure to output and write
        sdSolverPtr->outputModule().addField(pExact, "p_exact");
        sdSolverPtr->outputModule().write(1.0);
    }

    using std::sqrt;
    sumSDFluxNorms = sqrt(sumSDFluxNorms);
    sumSDIFFluxNorms = sqrt(sumSDIFFluxNorms);
    sumSDPressureNorms = sqrt(sumSDPressureNorms);
    std::cout << "Sub-domain pressure/flux/iFFlux norms: " << sumSDPressureNorms << "/"
                                                           << sumSDFluxNorms << "/"
                                                           << sumSDIFFluxNorms << std::endl;

    // compute error in mortar
    double sumMortarNorm = 0.0;
    for (auto ifPtr : interfaces)
    {
        // determine normal vector on interface (assumes no kinks per interface!)
        Dune::FieldVector<double, InterfaceGridView::dimensionworld> normal;
        const auto& posGG = ifPtr->positiveNeighborSolver().gridGeometry();
        const auto& posProblem = ifPtr->positiveNeighborSolver().problem();

        bool found = false;
        for (const auto& element : elements(posGG.gridView()))
        {
            auto fvGeometry = localView(posGG);
            fvGeometry.bindElement(element);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto boolIdxPair = posProblem.isOnMortarInterface(element, scvf);
                if (boolIdxPair.first && boolIdxPair.second == ifPtr->id())
                { normal = scvf.unitOuterNormal(); found = true; break; }
            }

            if (found) break;
        }

        if (!found) DUNE_THROW(Dune::InvalidStateException, "Could not determine normal vector");

        const auto& ifBasis = ifPtr->gridGeometry().feBasis();
        auto exactIF = [&] (const auto& pos) { return mv == OnePMortarVariableType::pressure ? posProblem.exact(pos)[0]
                                                                                             : posProblem.exactFlux(pos)*normal; };
        auto analyticIFGF = Dune::Functions::makeAnalyticGridViewFunction(exactIF, ifBasis.gridView());
        const auto norm = Dumux::integrateL2Error(ifBasis.gridView(), analyticIFGF, *interfaceGridFunctions[ifPtr->id()], order);
        sumMortarNorm += norm*norm;

        // add analytic solution and write
        auto& ifWriter = interfaceWriters[ifPtr->id()];
        auto& ifSeqWriter = interfaceSequenceWriters[ifPtr->id()];

        const auto info = Dune::VTK::FieldInfo(getMortarVariableName(mv) + "_exact", Dune::VTK::FieldInfo::Type::scalar, 1);
        if (InterfaceTraits::basisOrder == 0) ifWriter->addCellData(analyticIFGF, info);
        else ifWriter->addVertexData(analyticIFGF, info);

        std::cout << "Writing output for problem \"interface" << ifPtr->id() << "\". ";
        Dune::Timer writeTimer;
        ifSeqWriter.write(1.0);
        std::cout << "Took " << writeTimer.elapsed() << " seconds." << std::endl;
    }
    sumMortarNorm = sqrt(sumMortarNorm);

    // write error norms into file
    using std::sqrt;
    std::ofstream errorFile(getParam<std::string>("L2Error.OutputFile"), std::ios::app);
    errorFile << sumSDPressureNorms << ","
              << sumSDFluxNorms << ","
              << sumSDIFFluxNorms << ","
              << sumMortarNorm << std::endl;
    errorFile.close();
}

///////////////////////////////////////////////////////////////////
// Main Program. Selects the solvers etc to be passed to solve() //
///////////////////////////////////////////////////////////////////
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // TODO: Runtime support for box
    using TTDarcyTpfa = Properties::TTag::DarcyOnePTpfa;
    using Solver = Solver<TTDarcyTpfa>;

    ///////////////////////////////////////////////////////////////////////
    // Select the classes depending on input file setup and call solve() //
    ///////////////////////////////////////////////////////////////////////
    const auto mortarVariableType = getParam<std::string>("Mortar.VariableType");
    OnePMortarVariableType mvType = mortarVariableType == "Pressure" ? OnePMortarVariableType::pressure
                                                                     : OnePMortarVariableType::flux;

    // create sub-domain solvers
    std::size_t solverIdx = 0;
    std::vector<std::shared_ptr<Solver>> solvers;
    while (hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.File")
           || hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.Cells"))
    {
        solvers.emplace_back(std::make_shared<Solver>());
        solvers.back()->init("SubDomain" + std::to_string(solverIdx));
        solverIdx++;
    }

    // create interface grid geometries
    using InterfaceGrid = typename InterfaceTraits::Grid;
    using InterfaceGridGeometry = typename InterfaceTraits::GridGeometry;

    std::size_t interfaceIdx = 0;
    std::vector<GridManager<InterfaceGrid>> ifGridManagers;
    std::vector<std::shared_ptr<InterfaceGridGeometry>> ifGridGeometries;
    while (hasParamInGroup("Interface" + std::to_string(interfaceIdx), "Grid.File")
           || hasParamInGroup("Interface" + std::to_string(interfaceIdx), "Grid.Cells"))
    {
        ifGridManagers.emplace_back();
        ifGridManagers.back().init("Interface" + std::to_string(interfaceIdx));

        const auto& gridView = ifGridManagers.back().grid().leafGridView();
        auto feBasis = std::make_shared<typename InterfaceTraits::FEBasis>(gridView);
        ifGridGeometries.emplace_back( std::make_shared<InterfaceGridGeometry>(feBasis) );

        interfaceIdx++;
    }

    std::cout << "Created " << solverIdx << " subdomains and " << interfaceIdx << " interfaces" << std::endl;
    std::cout << "\n --- Spinning up iterative solver --- " << std::endl;

    const auto projectorType = getParam<std::string>("Mortar.ProjectorType");
    if (projectorType == "Flat")
    {
        using P = FlatProjector<typename InterfaceTraits::SolutionVector>;
        solveIteratively< P >(solvers, ifGridGeometries, mvType);
    }
    else if (projectorType == "Sharp")
    {
        if (mvType == OnePMortarVariableType::pressure)
            DUNE_THROW(Dune::InvalidStateException, "Pressure mortar does not work with sharp projection");

        using P = SharpProjector<typename InterfaceTraits::SolutionVector>;
        solveIteratively< P >(solvers, ifGridGeometries, mvType);
    }
    else
        DUNE_THROW(Dune::NotImplemented, "Other projector types");

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
