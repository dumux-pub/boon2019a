// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_MORTAR_DARCY_SUBPROBLEM_HH
#define DUMUX_MORTAR_DARCY_SUBPROBLEM_HH

#include <limits>
#include <unordered_map>

#include <dune/grid/yaspgrid.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "spatialparams.hh"

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include "interface.hh"
#include "mortarvariabletype.hh"
#include "traceoperatorhelper.hh"

namespace Dumux {
template <class TypeTag>
class DarcySubProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct DarcyOneP { using InheritsFrom = std::tuple<OneP>; };
struct DarcyOnePTpfa { using InheritsFrom = std::tuple<DarcyOneP, CCTpfaModel>; };
struct DarcyOnePMpfa { using InheritsFrom = std::tuple<DarcyOneP, CCMpfaModel>; };
struct DarcyOnePBox { using InheritsFrom = std::tuple<DarcyOneP, BoxModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyOneP> { using type = Dumux::DarcySubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyOneP>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::Constant<0, Scalar> > ;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<Scalar, 2>>;
};

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyOneP>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePDarcySpatialParams<GridGeometry, Scalar>;
};

// Some optimization flags
template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::DarcyOneP> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::DarcyOneP> { static constexpr bool value = false; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::DarcyOneP> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::DarcyOneP> { static constexpr bool value = false; };

} // end namespace Properties

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using CoupledScvfMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<GridGeometry>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    //! Export spatial parameters type
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    //! The constructor
    DarcySubProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                    std::shared_ptr<SpatialParams> spatialParams,
                    const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        const auto mv = getParamFromGroup<std::string>("Mortar", "VariableType");
        if (mv == "Pressure") mortarVariableType_ = OnePMortarVariableType::pressure;
        else if (mv == "Flux") mortarVariableType_ = OnePMortarVariableType::flux;
        else DUNE_THROW(Dune::NotImplemented, "Support for specified mortar variable type");
        useHomogeneousSetup_ = false;
    }

    //! Return the problem name.
    const std::string& name() const
    { return problemName_; }

    /*!
     * \name Problem parameters
     */
    // \{

    //! Returns the temperature within the domain in [K].
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolume& scv) const
    {
        if ( isOnMortarInterface(element, scv).first )
        {
            BoundaryTypes values;
            if (mortarVariableType_ == OnePMortarVariableType::pressure)
                values.setAllDirichlet();
            else
                values.setAllNeumann();
            return values;
        }
        else
            return boundaryTypesAtPos(scv.dofPosition());
    }

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        if ( isOnMortarInterface(element, scvf).first )
        {
            BoundaryTypes values;
            if (mortarVariableType_ == OnePMortarVariableType::pressure)
                values.setAllDirichlet();
            else
                values.setAllNeumann();
            return values;
        }
        else
            return boundaryTypesAtPos(scvf.ipGlobal());
    }

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    /*!
     * \brief Evaluate the exact solution at a given position.
     */
    NumEqVector exact(const GlobalPosition& globalPos) const
    {
        using std::sin;
        using std::cos;

        const auto x = globalPos[0];
        const auto y = globalPos[1];

        return NumEqVector( y*y*(1.0 - 1.0/3.0*y) + x*(1.0-x)*y*sin(2.0*M_PI*x) );
    }

    /*!
     * \brief Evaluate the exact normal velocity at a given position.
     */
    GlobalPosition exactFlux(const GlobalPosition& globalPos) const
    {
        using std::sin;
        using std::cos;

        const auto x = globalPos[0];
        const auto y = globalPos[1];
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);

        static const Scalar mu = getParam<Scalar>("0.Component.LiquidKinematicViscosity");
        return GlobalPosition( {-1.0*k/mu*(y*((1.0-2.0*x)*sin(2.0*x*M_PI) - 2.0*M_PI*(x-1.0)*x*cos(2.0*M_PI*x))),
                                -1.0*k/mu*(-(x-1.0)*x*sin(2*M_PI*x) - (y-2.0)*y)} );
    }

    /*!
     * \brief Evaluate the source term at a given position.
     */
    NumEqVector sourceAtPos(const GlobalPosition& globalPos) const
    {
        if (!useHomogeneousSetup_)
        {
            using std::sin;
            using std::cos;

            const auto x = globalPos[0];
            const auto y = globalPos[1];
            const auto& k = this->spatialParams().permeabilityAtPos(globalPos);

            static const Scalar rho = getParam<Scalar>("0.Component.LiquidDensity");
            static const Scalar mu = getParam<Scalar>("0.Component.LiquidKinematicViscosity");
            return NumEqVector( 2.0*k*rho/mu*( (y - 2.0*M_PI*M_PI*(x-1.0)*x*y)*sin(2.0*M_PI*x) + 2.0*M_PI*(2.0*x-1.0)*y*cos(2.0*M_PI*x) + y - 1.0) );
        }

        return NumEqVector(0.0);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet sub-control volume face
     * \note This overload is for the box scheme
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        const auto mortarIdPair = isOnMortarInterface(element, scv);
        if ( mortarIdPair.first )
            return PrimaryVariables( mortarProjections_.at(mortarIdPair.second)[scv.dofIndex()] );
        return dirichletAtPos(scv.dofPosition());
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet sub-control volume face
     * \note This overload is for cell-centered schemes
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto mortarIdPair = isOnMortarInterface(element, scvf);
        if ( mortarIdPair.first )
            return PrimaryVariables( mortarProjections_.at(mortarIdPair.second)[scvf.insideScvIdx()] );
        return dirichletAtPos(scvf.ipGlobal());
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet segment
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        if (!useHomogeneousSetup_)
            return exact(globalPos);
        else
            return PrimaryVariables(0.0);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        const auto mortarIdPair = isOnMortarInterface(element, scvf);
        if ( mortarIdPair.first )
        {
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            auto flux = mortarProjections_.at(mortarIdPair.second)[insideScv.elementIndex()];
            flux *= elemVolVars[insideScv].density();

            return isOnNegativeMortarSide(mortarIdPair.second) ? NumEqVector(-1.0*flux) : NumEqVector(flux);
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "This test should have no Neumann BCS");
    }

    // \}

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }


    //! set the projected mortar solution
    void setMortarProjection(std::size_t interfaceId, SolutionVector p)
    {
        mortarProjections_[interfaceId] = p;
    }

    //! Set whether or not the homogeneous system is solved
    void setUseHomogeneousSetup(bool value)
    {
        useHomogeneousSetup_ = value;
    }

    //! Returns true (plus the interface id) if scvf is on interface
    std::pair<bool, std::size_t> isOnMortarInterface(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        for (const auto& pair : coupledScvfMaps_)
        {
            auto it = pair.second.find(eIdx);
            if (it != pair.second.end())
            {
                const auto& scvfList = it->second;
                if ( std::find(scvfList.begin(),
                               scvfList.end(),
                               scvf.index()) != scvfList.end() )
                    return std::make_pair(true, pair.first);
            }
        }

        return std::make_pair(false, std::numeric_limits<std::size_t>::max());
    }

    //! Returns true (plus the interface id) if scv is on interface
    std::pair<bool, std::size_t> isOnMortarInterface(const Element& element, const SubControlVolume& scv) const
    { DUNE_THROW(Dune::NotImplemented, "Support for Box"); }

    //! Returns true if this domain is on the "negative" side of mortar
    bool isOnNegativeMortarSide(std::size_t interfaceId) const
    {
        auto it = isOnNegativeMortarSide_.find(interfaceId);
        if (it == isOnNegativeMortarSide_.end())
            DUNE_THROW(Dune::InvalidStateException, "Interface not registered");
        return it->second;
    }

    //! Define the side (positive/negative) this sub-domain is on with respect to interface
    void setInterfaceSide(std::size_t interfaceId, InterfaceSide side)
    {
        if (side == InterfaceSide::negative)
            isOnNegativeMortarSide_[interfaceId] = true;
        else if (side == InterfaceSide::positive)
            isOnNegativeMortarSide_[interfaceId] = false;
        else
            DUNE_THROW(Dune::InvalidStateException, "Unsupported interface type");
    }

    //! sets the map of scvfs living on the interface with id interfaceId
    void setCouplingScvfMap(std::size_t interfaceId, const CoupledScvfMap& map)
    {
        coupledScvfMaps_[interfaceId] = map;
    }

    //! add fields to the output module
    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule) const
    {
        for (auto& p : mortarProjections_)
            outputModule.addField(p.second, "mortar_" + std::to_string(p.first), OutputModule::FieldType::element);
    }

private:
    std::string problemName_;

    OnePMortarVariableType mortarVariableType_;
    std::unordered_map<std::size_t, CoupledScvfMap> coupledScvfMaps_;
    std::unordered_map<std::size_t, SolutionVector> mortarProjections_;
    std::unordered_map<std::size_t, bool> isOnNegativeMortarSide_;
    bool useHomogeneousSetup_;
};

} // end namespace Dumux

#endif //DUMUX_DARCY_SUBPROBLEM_HH
