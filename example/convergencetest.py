import os
import sys
import math
import copy
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# make sure user input is correct
if len(sys.argv) < 3 or (sys.argv[1] != '2' and sys.argv[1] != '4'):
    sys.stderr.write( "Please provide two runtime arguments:\n" \
                      "\t1. Provide 2 or 4 to specify if 2 or 4 subdomains should be considered\n" \
                      "\t2. Provide an integer number specifying the number of grid refinement steps\n" )
    sys.exit(1)

# parse user input
numSubDomains = int(sys.argv[1])
numRefinements = int(sys.argv[2])

# define initial grid resolutions for all subdomains
cells = []
cellsMortar = []

# define inital x-resolution of the first two sub-domains
# separately such that this script works in both cases
initialNumCellsX1 = 10
initialNumCellsX2 = 11
initialNumCellsY1 = 10
initialNumCellsY2 = 11
if numSubDomains == 4:
    initialNumCellsX1 = 5
    initialNumCellsX2 = 7
    initialNumCellsY1 = 9
    initialNumCellsY2 = 13

cells.append([initialNumCellsX1, initialNumCellsY1])
cells.append([initialNumCellsX2, initialNumCellsY2])
cells.append([initialNumCellsX1, initialNumCellsY2])
cells.append([initialNumCellsX2, initialNumCellsY1])

initialNumCellsMortar = 4
if numSubDomains == 4:
    initialNumCellsMortar = 2

cellsMortar.append(initialNumCellsMortar)
cellsMortar.append(initialNumCellsMortar)
cellsMortar.append(initialNumCellsMortar)
cellsMortar.append(initialNumCellsMortar)


########################
# Run convergence test #
########################
if numSubDomains == 2:
    subDomainDx = 1.0
    subDomainDy = 1.0
    mortarDx = 1.0
    inputFileName = "params_2domain.input"
    numInterfaces = 1
else:
    subDomainDx = 0.5
    subDomainDy = 1.0
    mortarDx = 0.5
    inputFileName = "params_4domain.input"
    numInterfaces = 4

# (maybe) remove error norm files from previous runs
errorFileFlat = "errors_flat.csv"
errorFileSharp = "errors_sharp.csv"
if os.path.exists(errorFileFlat):
    subprocess.call(["rm", errorFileFlat])
if os.path.exists(errorFileSharp):
    subprocess.call(["rm", errorFileSharp])

# make sure executable is up to date
print("\nCompiling the executable\n")
subprocess.call(["make", "example"])
print("\n")

# make sure executable exists
if not os.path.exists("example"):
    sys.stderr.write("Could not find executable. Aborting.")
    sys.exit(1)

# for each refinement, compute the average discretization length
# and call dumux twice, once with the flat, and once with the sharp projection
hAvg = []
hAvgMortar = []
for refIdx in range(1, numRefinements+2):
    factor = math.pow(2, refIdx-1)

    curCells = []
    curHAvg = 0.0
    for i in range(0, numSubDomains):
        curCells.append([ int(cells[i][0]*factor), int(cells[i][1]*factor) ])
        curHAvg += 0.5*(subDomainDx/int(cells[i][0]*factor) + subDomainDy/int(cells[i][1]*factor))

    curCellsMortar = []
    curHAvgMortar = 0.0
    for i in range(0, numInterfaces):
        curCellsMortar.append( int(cellsMortar[i]*factor) )
        curHAvgMortar += mortarDx/int(cellsMortar[i]*factor)

    curHAvg /= numSubDomains
    curHAvgMortar /= numInterfaces

    hAvg.append(curHAvg)
    hAvgMortar.append(curHAvgMortar)

    # prepare call for dumux (number of cells etc)
    runTimeArgs = []
    runTimeArgs.append("./example")
    runTimeArgs.append(inputFileName)
    for i in range(0, numSubDomains):
        runTimeArgs.append("-SubDomain" + str(i) + ".Grid.Cells")
        runTimeArgs.append(str(curCells[i][0]) + " " + str(curCells[i][1]))
    for i in range(0, numInterfaces):
        runTimeArgs.append("-Interface" + str(i) + ".Grid.Cells")
        runTimeArgs.append(str(curCellsMortar[i]))

    # call to dumux using flat projection
    flatArgs = copy.deepcopy(runTimeArgs)
    for i in range(0, numSubDomains):
        flatArgs.append("-SubDomain" + str(i) + ".Problem.Name")
        flatArgs.append("subdomain" + str(i) + "_" + str(refIdx) + "_flat")
    for i in range(0, numInterfaces):
        flatArgs.append("-Interface" + str(i) + ".Problem.Name")
        flatArgs.append("interface" + str(i) + "_" + str(refIdx) + "_flat")
    flatArgs.append("-Mortar.ProjectorType")
    flatArgs.append("Flat")
    flatArgs.append("-L2Error.OutputFile")
    flatArgs.append(errorFileFlat)

    print("\n\nSolving with flat projection for refinement " + str(refIdx-1))
    subprocess.call(flatArgs)

    # call to dumux using flat projection
    sharpArgs = copy.deepcopy(runTimeArgs)
    for i in range(0, numSubDomains):
        sharpArgs.append("-SubDomain" + str(i) + ".Problem.Name")
        sharpArgs.append("subdomain" + str(i) + "_" + str(refIdx) + "_sharp")
    for i in range(0, numInterfaces):
        sharpArgs.append("-Interface" + str(i) + ".Problem.Name")
        sharpArgs.append("interface" + str(i) + "_" + str(refIdx) + "_sharp")
    sharpArgs.append("-Mortar.ProjectorType")
    sharpArgs.append("Sharp")
    sharpArgs.append("-L2Error.OutputFile")
    sharpArgs.append(errorFileSharp)

    print("\n\nSolving with sharp projection for refinement " + str(refIdx-1))
    subprocess.call(sharpArgs)


# compute the rates
errorsFlat = np.loadtxt(errorFileFlat, delimiter=',')
errorsSharp = np.loadtxt(errorFileSharp, delimiter=',')

# compute rates
ratesFlatPressure = []
ratesFlatFlux = []
ratesFlatIFFlux = []
ratesFlatMortar = []
ratesSharpPressure = []
ratesSharpFlux = []
ratesSharpIFFlux = []
ratesSharpMortar = []

if (len(errorsFlat) != len(errorsSharp)):
    sys.stderr.write("Number of refinements not equal for flat and sharp!!")
    sys.exit(1)

for i in range(0, len(errorsFlat)-1):
    deltaH = np.log(hAvg[i+1]) - np.log(hAvg[i])

    deltaEP = np.log(errorsFlat[i+1][0]) - np.log(errorsFlat[i][0])
    deltaEF = np.log(errorsFlat[i+1][1]) - np.log(errorsFlat[i][1])
    deltaEIF = np.log(errorsFlat[i+1][2]) - np.log(errorsFlat[i][2])
    deltaEM = np.log(errorsFlat[i+1][3]) - np.log(errorsFlat[i][3])

    ratesFlatPressure.append( deltaEP / deltaH )
    ratesFlatFlux.append( deltaEF / deltaH )
    ratesFlatIFFlux.append( deltaEIF / deltaH )
    ratesFlatMortar.append( deltaEM / deltaH )

    deltaEP = np.log(errorsSharp[i+1][0]) - np.log(errorsSharp[i][0])
    deltaEF = np.log(errorsSharp[i+1][1]) - np.log(errorsSharp[i][1])
    deltaEIF = np.log(errorsSharp[i+1][2]) - np.log(errorsSharp[i][2])
    deltaEM = np.log(errorsSharp[i+1][3]) - np.log(errorsSharp[i][3])

    ratesSharpPressure.append( deltaEP / deltaH )
    ratesSharpFlux.append( deltaEF / deltaH )
    ratesSharpIFFlux.append( deltaEIF / deltaH )
    ratesSharpMortar.append( deltaEM / deltaH )

# print results to terminal
print("h:              ", hAvg)

print("\n")
print("Flat pressure:  ", ratesFlatPressure)
print("Flat flux:      ", ratesFlatFlux)
print("Flat ifFlux:    ", ratesFlatIFFlux)
print("Flat mortar:    ", ratesFlatMortar)

print("\n")
print("Sharp pressure: ", ratesSharpPressure)
print("Sharp flux:     ", ratesSharpFlux)
print("Sharp ifFlux:   ", ratesSharpIFFlux)
print("Sharp mortar:   ", ratesSharpMortar)

# plot pressure error norms
plt.figure(1)
plt.loglog(1.0/np.array(hAvg), errorsFlat[:, 0], label='flat')
plt.loglog(1.0/np.array(hAvg), errorsSharp[:, 0], label='sharp')
plt.legend()
plt.xlabel(r"$1/h$")
plt.ylabel(r"$\| p - p_h \|$")
plt.savefig("convergence_pressure.pdf", bbox_inches='tight')

# plot flux error norms
plt.figure(2)
plt.loglog(1.0/np.array(hAvg), errorsFlat[:, 1], label='flat')
plt.loglog(1.0/np.array(hAvg), errorsSharp[:, 1], label='sharp')
plt.legend()
plt.xlabel(r"$1/h$")
plt.ylabel(r"$\| \left( \mathbf{u} - \mathbf{u}_h \right) \, \mathbf{n} \|$")
plt.savefig("convergence_flux.pdf", bbox_inches='tight')

# plot interface flux error norms
plt.figure(3)
plt.loglog(1.0/np.array(hAvg), errorsFlat[:, 2], label='flat')
plt.loglog(1.0/np.array(hAvg), errorsSharp[:, 2], label='sharp')
plt.legend()
plt.xlabel(r"1/h")
plt.ylabel(r"$\| \left( \mathbf{u} - \mathbf{u}_h \right) \, \mathbf{n} \|_\Gamma$")
plt.savefig("convergence_ifflux.pdf", bbox_inches='tight')

# plot flux error norms
plt.figure(4)
plt.loglog(1.0/np.array(hAvg), errorsFlat[:, 3], label='flat')
plt.loglog(1.0/np.array(hAvg), errorsSharp[:, 3], label='sharp')
plt.legend()
plt.xlabel(r"1/h")
plt.ylabel(r"$\| \lambda - \lambda_h \|$")
plt.savefig("convergence_mortar.pdf", bbox_inches='tight')
