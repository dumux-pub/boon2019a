// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_ONEP_INTERFACE_OPERATOR_HH
#define DUMUX_MORTAR_ONEP_INTERFACE_OPERATOR_HH

#include <dune/istl/operators.hh>

#include "projector.hh"
#include "mortarvariabletype.hh"

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class Interface, class SolutionVector>
void extractInterfaceSolutions(std::vector<std::shared_ptr<Interface>> interfaces,
                               std::vector<SolutionVector>& solutionVectors,
                               const SolutionVector& solution)
{
    // TODO: ASSERTION ON VECTOR SIZE
    std::size_t curIdx = 0;
    solutionVectors.resize(interfaces.size());
    for (std::size_t i = 0; i < interfaces.size(); ++i)
    {
        solutionVectors[i].resize(interfaces[i]->gridGeometry().numDofs());
        for (std::size_t j = 0; j < interfaces[i]->gridGeometry().numDofs(); ++j)
            solutionVectors[i][j] = solution[curIdx++];
    }
}

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class Interface,
         class SubDomainSolver,
         class MortarSolutionVector>
class OnePMortarInterfaceOperator
: public Dune::LinearOperator< MortarSolutionVector, MortarSolutionVector >
{
    using FieldType =  typename MortarSolutionVector::field_type;

    using SDSolverPtr = std::shared_ptr<SubDomainSolver>;
    using InterfacePtr = std::shared_ptr<Interface>;

public:
    explicit OnePMortarInterfaceOperator(std::vector<InterfacePtr> interfacePtrs,
                                         std::vector<SDSolverPtr> sdSolverPtrs,
                                         OnePMortarVariableType mv)
    : interfacePtrs_(interfacePtrs)
    , sdSolverPtrs_(sdSolverPtrs)
    , variableType_(mv)
    {}

    //! apply operator to x:  \f$ y = A(x) \f$
    virtual void apply(const MortarSolutionVector& x, MortarSolutionVector& r) const
    {
        // extract interface sub-solutions
        std::vector<typename Interface::Traits::SolutionVector> ifSolutions;
        extractInterfaceSolutions(interfacePtrs_, ifSolutions, x);

        // project mortar variable into sub-domains (let detail impl decide how)
        for (std::size_t i = 0; i < interfacePtrs_.size(); ++i)
        {
            MortarSolutionVector p1, p2;
            ProjectionDetail::projectToSubDomain(interfacePtrs_[i]->positiveProjector(),
                                                 interfacePtrs_[i]->negativeProjector(),
                                                 ifSolutions[i], p1, p2);

            interfacePtrs_[i]->positiveNeighborSolver().problemPointer()->setMortarProjection(i, p1);
            interfacePtrs_[i]->negativeNeighborSolver().problemPointer()->setMortarProjection(i, p2);
        }

        // solve the sub-domains
        for (auto sdSolverPtr : sdSolverPtrs_)
            sdSolverPtr->solve();

        // compute the solution or flux jump across the interfaces
        r.resize(x.size());
        std::size_t curIdx = 0;
        for (std::size_t i = 0; i < interfacePtrs_.size(); ++i)
        {
            const auto& interface = *interfacePtrs_[i];

            const auto& posSolver = interface.positiveNeighborSolver();
            const auto& posTraceOp = interface.positiveTraceOperator();
            const auto& posProjector = interface.positiveProjector();

            const auto& negSolver = interface.negativeNeighborSolver();
            const auto& negTraceOp = interface.negativeTraceOperator();
            const auto& negProjector = interface.negativeProjector();

            MortarSolutionVector t1, t2;
            if (variableType_ == OnePMortarVariableType::pressure)
            {
                t1 = posTraceOp.recoverTraceFlux(posSolver.solution());
                t2 = negTraceOp.recoverTraceFlux(negSolver.solution());
            }
            else if (variableType_ == OnePMortarVariableType::flux)
            {
                t1 = posTraceOp.recoverTraceSolution(posSolver.solution());
                t2 = negTraceOp.recoverTraceSolution(negSolver.solution());
                t2 *= -1.0;
            }
            else
                DUNE_THROW(Dune::InvalidStateException, "Unkown mortar variable type");

            // Let the detail implementation decide how to project back
            MortarSolutionVector p1, p2;
            ProjectionDetail::projectFromSubDomain(posProjector, negProjector, t1, t2, p1, p2);

            if (p1.size() != p2.size())
                DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have the same size");
            if (p1.size() != interface.gridGeometry().numDofs())
                DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have size of interface space");

            for (std::size_t j = 0; j < p1.size(); ++j)
            {
                r[curIdx] = p1[j];
                r[curIdx] += p2[j];
                curIdx++;
            }
        }
    }

    //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
    virtual void applyscaleadd(FieldType alpha, const MortarSolutionVector& x, MortarSolutionVector& y) const
    {
        MortarSolutionVector yTmp;

        apply(x, yTmp);
        yTmp *= alpha;

        y += yTmp;
    }

    //! Category of the solver (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    { return Dune::SolverCategory::sequential; }

private:
    std::vector<InterfacePtr> interfacePtrs_;
    std::vector<SDSolverPtr> sdSolverPtrs_;
    OnePMortarVariableType variableType_;
};

} // end namespace Dumux

#endif
