Welcome to the dumux pub table Boon2019a
========================================

This module contains the source code for the examples in the paper

Wietse M Boon, Dennis Gläser, Rainer Helmig, Ivan Yotov, [Flux-mortar mixed finite element methods on non-matching grids](https://arxiv.org/pdf/2008.09372.pdf), 2020 

Installation instruction (__for linux__)
----------------------------------------

For building the code from source, create a new folder and download the `installBoon2019a.sh` script from this repository into the newly created folder. Then, type

```sh
chmod +x installBoon2019a.sh
./installBoon2019a.sh
```
to download and configure all dune dependencies and dumux. This might take a while. Please note that the following requirements need to be installed:

- CMake (>2.8.12)
- C, C++ compiler (C++14 required)
- Fortran compiler (gfortran)
- UMFPack from SuiteSparse
- Python (+ matplotlib, numpy)

After successfully running the installation script, navigate to the folder containing the example:

```sh
cd boon2019a/build-cmake/example/
```

You can then run the convergence test by typing

```sh
python convergencetest.py "numSubDomains" "numRefinements"
```

where "numSubDomains" is to be substituted by the number of subdomains you want to consider. You can choose between "2" or "4" to run the two examples shown in the paper. The second argument "numRefinements" should be substituted by an integer number > 1 and defines how many grid refinements should be done. Thus, to reproduce the examples in the paper (2/4 subdomains with 5 refinements), type

```sh
python convergencetest.py 2 5
```

or

```sh
python convergencetest.py 4 5
```

Note that running the script `convergencetest.py` will first compile the executable, which might take a minute. If you want to change the initial grid resolution, open `convergencetest.py` and modify the lines 26-44 according to your wishes.


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Boon2019a
cd Boon2019a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/boon2019a/-/raw/master/docker_boon2019a.sh
```

Open the Docker Container
```bash
bash docker_boon2019a.sh open
```

After the script has run successfully, you may build the executable

```bash
cd boon2019a/build-cmake
make build_tests
```

and it will be located in the build-cmake/example folder. It can be executed with an input file by running

```bash
cd example
./example params_4domain.input
```
