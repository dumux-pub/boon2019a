#!/bin/bash

################################################################################
# This script installs the module "Boon2019a" together with all dependencies.

# defines a function to exit with error message
exitWith ()
{
    echo "\n$1"
    exit 1
}

# Everything will be installed into a newly created sub-folder named "Boon2019a".
echo "Creating the folder Boon2019a to install the modules in"
if ! mkdir -p Boon2019a; then exitWith "--Error: could not create top folder Boon2019a"; fi
if ! cd Boon2019a; then exitWith "--Error: could not enter top folder Boon2019a"; fi

# Boon2019a
# master # 5dc38206f3bddf558a651de9e694f0bc46ced921 # 2020-05-29 11:04:36 +0200 # Dennis.Glaeser
if ! git clone https://git.iws.uni-stuttgart.de/dumux-pub/boon2019a.git; then exitWith "-- Error: failed to clone Boon2019a."; fi
if ! cd boon2019a; then exitWith "-- Error: could not enter folder boon2019a."; fi
if ! git checkout master; then exitWith "-- Error: failed to check out branch master in module Boon2019a."; fi
if ! git reset --hard 51bef4268153b58e34d1f07a3619b8413a72baa1; then exitWith "-- Error: failed to check out commit 51bef4268153b58e34d1f07a3619b8413a72baa1 in module Boon2019a."; fi
echo "-- Successfully set up the module Boon2019a\n"
cd ..

# dune-common
# releases/2.6 # 048782002f64b9c2c83ba22774677aa14ed4d38f # 2020-06-26 18:12:27 +0000 # Dominic Kempf
if ! git clone https://gitlab.dune-project.org/core/dune-common.git; then exitWith "-- Error: failed to clone dune-common."; fi
if ! cd dune-common; then exitWith "-- Error: could not enter folder dune-common."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-common."; fi
if ! git reset --hard 048782002f64b9c2c83ba22774677aa14ed4d38f; then exitWith "-- Error: failed to check out commit 048782002f64b9c2c83ba22774677aa14ed4d38f in module dune-common."; fi
echo "-- Successfully set up the module dune-common\n"
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
if ! git clone https://gitlab.dune-project.org/core/dune-geometry.git; then exitWith "-- Error: failed to clone dune-geometry."; fi
if ! cd dune-geometry; then exitWith "-- Error: could not enter folder dune-geometry."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-geometry."; fi
if ! git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38; then exitWith "-- Error: failed to check out commit 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 in module dune-geometry."; fi
echo "-- Successfully set up the module dune-geometry\n"
cd ..

# dune-grid
# releases/2.6 # f7983c01afbe9b7c379f9944ecbacb189ac8c6e6 # 2020-02-09 10:28:23 +0000 # Christoph Grüninger
if ! git clone https://gitlab.dune-project.org/core/dune-grid.git; then exitWith "-- Error: failed to clone dune-grid."; fi
if ! cd dune-grid; then exitWith "-- Error: could not enter folder dune-grid."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-grid."; fi
if ! git reset --hard f7983c01afbe9b7c379f9944ecbacb189ac8c6e6; then exitWith "-- Error: failed to check out commit f7983c01afbe9b7c379f9944ecbacb189ac8c6e6 in module dune-grid."; fi
echo "-- Successfully set up the module dune-grid\n"
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
if ! git clone https://gitlab.dune-project.org/core/dune-localfunctions.git; then exitWith "-- Error: failed to clone dune-localfunctions."; fi
if ! cd dune-localfunctions; then exitWith "-- Error: could not enter folder dune-localfunctions."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-localfunctions."; fi
if ! git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f; then exitWith "-- Error: failed to check out commit ee794bfdfa3d4f674664b4155b6b2df36649435f in module dune-localfunctions."; fi
echo "-- Successfully set up the module dune-localfunctions\n"
cd ..

# dune-istl
# releases/2.6 # 3f96dfcc2fc741e0a87d392adc780b2beec4c91a # 2020-09-24 12:16:13 +0000 # Oliver Sander
if ! git clone https://gitlab.dune-project.org/core/dune-istl.git; then exitWith "-- Error: failed to clone dune-istl."; fi
if ! cd dune-istl; then exitWith "-- Error: could not enter folder dune-istl."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-istl."; fi
if ! git reset --hard 3f96dfcc2fc741e0a87d392adc780b2beec4c91a; then exitWith "-- Error: failed to check out commit 3f96dfcc2fc741e0a87d392adc780b2beec4c91a in module dune-istl."; fi
echo "-- Successfully set up the module dune-istl\n"
cd ..

# dune-foamgrid
# releases/2.6 # 20f7851cac039627402419db6cb5850b4485c871 # 2019-05-01 18:45:26 +0000 # Timo Koch
if ! git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git; then exitWith "-- Error: failed to clone dune-foamgrid."; fi
if ! cd dune-foamgrid; then exitWith "-- Error: could not enter folder dune-foamgrid."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-foamgrid."; fi
if ! git reset --hard 20f7851cac039627402419db6cb5850b4485c871; then exitWith "-- Error: failed to check out commit 20f7851cac039627402419db6cb5850b4485c871 in module dune-foamgrid."; fi
echo "-- Successfully set up the module dune-foamgrid\n"
cd ..

# dune-typetree
# releases/2.6 # 65f1eeff48260db6a6d7bb86695da4d7332adbc0 # 2018-12-11 14:27:43 +0100 # Steffen Müthing
if ! git clone https://gitlab.dune-project.org/staging/dune-typetree.git; then exitWith "-- Error: failed to clone dune-typetree."; fi
if ! cd dune-typetree; then exitWith "-- Error: could not enter folder dune-typetree."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-typetree."; fi
if ! git reset --hard 65f1eeff48260db6a6d7bb86695da4d7332adbc0; then exitWith "-- Error: failed to check out commit 65f1eeff48260db6a6d7bb86695da4d7332adbc0 in module dune-typetree."; fi
echo "-- Successfully set up the module dune-typetree\n"
cd ..

# dune-functions
# releases/2.6 # 12ec587ec35118a00fada8ab411719e76c0b9068 # 2020-09-06 19:50:32 +0000 # Simon Praetorius
if ! git clone https://gitlab.dune-project.org/staging/dune-functions.git; then exitWith "-- Error: failed to clone dune-functions."; fi
if ! cd dune-functions; then exitWith "-- Error: could not enter folder dune-functions."; fi
if ! git checkout releases/2.6; then exitWith "-- Error: failed to check out branch releases/2.6 in module dune-functions."; fi
if ! git reset --hard 12ec587ec35118a00fada8ab411719e76c0b9068; then exitWith "-- Error: failed to check out commit 12ec587ec35118a00fada8ab411719e76c0b9068 in module dune-functions."; fi
if [ -f ../boon2019a/patches/dune-functions_uncommitted.patch ]; then
    if ! git apply ../boon2019a/patches/dune-functions_uncommitted.patch; then exitWith "--Error: failed to apply patch ../boon2019a/patches/dune-functions_uncommitted.patch in module dune-functions."; fi
else
    exitWith "--Error: patch ../boon2019a/patches/dune-functions_uncommitted.patch was not found."
fi
echo "-- Successfully set up the module dune-functions\n"
cd ..

# dumux
# master # 1936a09488c207121ce4ec78696c850a246ad931
if ! git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git; then exitWith "-- Error: failed to clone dumux."; fi
if ! cd dumux; then exitWith "-- Error: could not enter folder dumux."; fi
if ! git checkout master; then exitWith "-- Error: failed to check out branch master in module dumux."; fi
if ! git reset --hard 1936a09488c207121ce4ec78696c850a246ad931; then exitWith "-- Error: failed to check out commit 1936a09488c207121ce4ec78696c850a246ad931 in module dumux."; fi
echo "-- Successfully set up the module dumux\n"
cd ..

echo "-- All modules haven been cloned successfully. Configuring project..."
if ! ./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all; then exitWith "--Error: could not configure project"; fi

echo "-- Configuring successful. Compiling applications..."
if ! cd boon2019a/build-cmake; then exitWith "--Error: could not enter build directory at boon2019a/build-cmake"; fi
if ! make build_tests; then exitWith "--Error: applications could not be compiled. Please try to compile them manually."; fi
